const express = require("express");
const hbs = require("hbs");
const path = require("path");
const fetch = require("node-fetch");
const fs = require("fs");

const app = express();
const PORT = 3000;

app.set('view engine', 'hbs');
hbs.registerPartials(path.join(__dirname, 'views', 'partials'));

app.use(express.static(path.join(__dirname, 'public')));

let cities = [];

// Зчитування міст з JSON файлу синхронно перед запуском сервера
fs.readFile(path.join(__dirname, 'cities.json'), 'utf-8', (err, data) => {
    if (err) {
        console.error("Помилка читання cities.json:", err);
        return;
    }
    cities = JSON.parse(data);
});

app.get('/', (req, res) => {
    res.render("Home");
});

app.get('/weather', (req, res) => {
    res.render("weather", { cities });
});

app.get('/weather/:city', async (req, res) => {
    let city = req.params.city;
    let apiKey = '9c2970c7f9737e7bd9f1ae72e9b9d38a';
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`;
    try {
        let response = await fetch(url);
        let weather = await response.json();
        res.render('weather', { city, weather, cities });
    } catch (error) {
        console.error(error);
        res.render("Помилка", { message: "Не вдалося отримати дані про погоду.", cities });
    }
});

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`);
});
